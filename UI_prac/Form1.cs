﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using LiveCharts;
using LiveCharts.Wpf;
using Brushes = System.Windows.Media.Brushes;
using Color = System.Windows.Media.Color;




namespace UI_prac
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            //Load += new EventHandler(Form1_Load);
            this.WindowState = FormWindowState.Maximized;
            barcode_uc1.Location = mainP.Location;
            barcode_uc1.Size = mainP.Size;
        }

        public List<dataList> DataList = new List<dataList>();

        public class dataList
        {
            public double count;
            public double Infeedno;
            // public DateTime time;
        }
        //get data for one table//
        private List<dataList> table1()
        {
            SqlConnection cnn = new SqlConnection("Data Source=DESKTOP-H6K123N\\SQLEXPRESS;Initial Catalog=ahsen;Integrated Security=True");
            cnn.Open();
            SqlCommand cmd = new SqlCommand("Select count(*) as TotalCount,InfeedNo from general_reporting_sorterInfeeds group by InfeedNo", cnn);
            //SqlCommand cmd = new SqlCommand("Select count(InfeedNo) as TotalCount,InfeedNo,DateTime from general_reporting_sorterInfeeds group by InfeedNo,DateTime", cnn);
            SqlDataReader dr = cmd.ExecuteReader();
            List<dataList> datalist = new List<dataList>();

            while (dr.Read())
            {
                datalist.Add(new dataList()
                {
                    count = Convert.ToDouble(dr["TotalCount"]),
                    Infeedno = Convert.ToDouble(dr["InfeedNo"]),
                    //time = Convert.ToDateTime((dr["DateTime"]))
                });
            }


            return datalist;
        }
        

        private void button1_Click(object sender, EventArgs e) //home BTN//
        {
            hidepanels();
            mainP.Visible = true;
        }

        public void Form1_resize()
        {
            Rectangle workArea = Screen.GetWorkingArea(this);
            //this.Size = new Size(workArea.Width,workArea.Height);
            // this.StartPosition = FormStartPosition.CenterParent;
            //this.panel2.Location = new Point(312, 48);
            //this.panel2.Size = new Size(workArea.Width - 300, workArea.Height - 150);
            //this.closeBTN.Location = new Point(workArea.Width - 10, 5);
        }


        private void closeBTN_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void load_chart()
        {
            DataList = table1();
        }

        //panels//
        public void hidepanels()
        {
            mainP.Visible = false;
            barcode_uc1.Visible = false;

        }

        //panels ends//

        private void Form1_Load(object sender, EventArgs e)
        {
            //settings before form load//
            hidepanels();
            mainP.Visible = true;
            
            // barcodeP.Visible = false;
            //panel21.Visible = false;
            //settings before form load ends// can be robust//

            //data//
            DataList = table1();
            ChartValues<double> values = new ChartValues<double>();
            List<String> lables = new List<string>();
            foreach (var item in DataList)
            {
                values.Add(item.count);
                lables.Add(item.Infeedno.ToString());
            }

            //data to graph//
            cartesianChart1.Series = new SeriesCollection
            {
                new ColumnSeries
                {
                    Title = "TITLE-1",
                    Values = values,
                    DataLabels = true,
                    Fill = System.Windows.Media.Brushes.BlueViolet
                    
                }
            };

            //clear axis before formating new one//
            //cartesianChart1.AxisX.Clear();
            //cartesianChart1.AxisY.Clear();
           
            //x axis labels//
            cartesianChart1.AxisX.Add(new Axis
            {
                Title = "Infeed No.",
                Labels = lables,
                Unit = 1,
                Separator = new Separator // force the separator step to 1, so it always display all labels
                {
                    StrokeThickness = 0,
                    Step = 1,
                    IsEnabled = false, //disable it to make it invisible.
                    Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(100, 100, 100))
                }
            });
            cartesianChart1.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(5, 5, 5));

            //y axis label//
            cartesianChart1.AxisY.Add(new Axis
            {
                Title = "Count",
                LabelFormatter = value => value.ToString(),
                Separator = new Separator { StrokeThickness = 1.5,
                Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(0,0,0)) } //change the gridline color

            });

            //graph2//
            cartesianChart2.Series = new SeriesCollection
            {
                new ColumnSeries
                {
                    Title = "TITLE-1",
                    Values = values,
                    DataLabels = true,
                    Fill = System.Windows.Media.Brushes.BlueViolet

                }
            };
            //clear axis before creating new formate//
            //cartesianChart2.AxisX.Clear();
            //cartesianChart2.AxisY.Clear();
            //x axis labels//
            cartesianChart2.AxisX.Add(new Axis
            {
                Title = "Infeed No.",
                Labels = lables,
                Unit = 1,
                Separator = new Separator // force the separator step to 1, so it always display all labels
                {
                    //StrokeThickness = 0,
                    Step = 1,
                    IsEnabled = false, //disable it to make it invisible.
                    Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(255, 255, 255))
                }
            });
            //cartesianChart1.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(5, 5, 5));

            //y axis label//
            cartesianChart2.AxisY.Add(new Axis
            {
                Title = "Count",
                LabelFormatter = value => value.ToString(),
                Separator = new Separator
                {
                    //StrokeThickness = 1.5,
                    Stroke = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromRgb(100, 100, 100))
                } //change the gridline color

            });
        }

        private void button2_Click(object sender, EventArgs e) //barcode btn//
        {
            hidepanels();
            barcode_uc1.Visible = true;
        }
    }
}
